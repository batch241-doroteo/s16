// alert("Hello, Batch 241!");

// ARITHMETIC OPERATORS
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum)

// Subtraction Operator
let difference = x - y;
console.log("Result of subtraction operator: " + difference)

// Multiplication Operator
let product = x * y;
console.log(" Result of multiplication operator :" + product);

// Division Operator
let quotient = x / y;
console.log(" Result of division operator :" + quotient);

// Modulo Operator
let remainder = y % x;
console.log(" Result of Modulo operator :" + remainder);

// Assignment Operator
// Basic assignment operator (=)
// the assignment operator adds the value of the right operand to a variable and assigns the result to the variable
let assignmentNumber = 8;

// Addition assignment operator
// Uses the current value of the variable and it ADDS a number (2) to itself. Afterwards, it reassigns to its new value.
// assignmentNumber = assignmentNumber + 2
assignmentNumber += 2;
console.log("This is the result of the assignment assignment operator: " + assignmentNumber)

// Subtraction/Multiplication/Division (-=, *=, /=)

// Subraction Assignment operator
assignmentNumber -= 2;
console.log("Result of the subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of the multiplication assignment assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of the division assignment assignment operator: " + assignmentNumber);

// Multiple operators and Parentheses
/*
	When multiple operators are applied in a single statement, it follows the PEMDAS rule (Parenthesis, exponents, multiplication, division, addition and subtraction)
	1. 3*4=12
	2. 12/5
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log ("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log ("Result of pemdas operation: " + pemdas);

// INCREMENT AND DECREMENT
//Operators that add or subtractt values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

// Pre-Increment
/*let preIncrement = ++z;
console.log("Result of the pre-increment: " + ipreIncrement);

console.log("Result of the pre-increment: " + z);*/

// Post-increment
// the value of "z" is returned and stored in the variable "postIncrement" then the value of "z" is increased by one.

let postIncrement = z++;
console.log("Result of the post-increment: " + postIncrement);
console.log("Result of the post-increment: " + z);

let a = 2;

/*// Pre-decrement
let preDecrement = --a;
console.log("Result of the pre-decrement: " + preDecrement);
console.log("Result of the pre-decrement: " + a);*/

// Post-decrement
let postDecrement = a--;
console.log("Result of the post-decrement: " + postDecrement);
console.log("Result of the post-decrement: " + a);


// TYPE COERCION
/*
	Type coercion is the automatic or implicit conversion of values from one data type to another
*/
let numA = '10'; 
let numB = 12;

/*
	Adding/concatenating a string and a number will result as a string
*/
let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let coercion1 = numA - numB;
console.log(coercion1);
console.log(typeof coercion);

// Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Addition of Number and Boolean
/*
	The result is a number
	The boolean "true" is associated with the value of 1.
*/

let numE = true + 1;
console.log(numE); //2
console.log(typeof numE); //number

let numF = false + 1;
console.log(numF); //1
console.log(typeof numE); //number

// Comparison operator
let juan = 'juan';

// equality operator(==)
/*
	Checks whether the operands are equal.hav ethe same content
	Attempts to Convert and Compare operands of different data types.
	- returns a boolean value (true/false)
*/

console.log (1 == 1);
console.log (1 == 2);
console.log (1 == '1');
console.log (1 == true);
// compare two strings that are the same
console.log('juan' == 'juan')
console.log ('true' == true);
console.log(juan == 'juan');

// Inequality Operator
/*
	Checks whether the operands are not equal/have different content
	Attempts to convert and compare operands of different data types
*/

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(1 != true);
console.log('juan' != 'juan');
console.log('juan' != juan);

// Strict Equality Operator (===)
/*
	Checks whether the operands are equal/have the same content
	Also COMPARES the date types of 2 values.
*/

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(1 === true);
console.log('juan' === 'juan');
console.log('juan' === juan);

// Strict Inequality operator (!==)
/*
	Checks whether the operands are not equal/have the same content
	Also COMPARES the data types
*/
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(1 !== true);
console.log('juan' !== 'juan');
console.log('juan' !== juan);

// Relational Operator
// returns a boolean value
let j = 50;
let k = 65;

// Greater than operator (>)
let isGreaterThan = j > k;
console.log(isGreaterThan)

// Less than operator (<)
let isLessThan = j < k;
console.log(isLessThan)

// Greater than operator or equal (>)
let isGTorEqual = j >= k;
console.log(isGTorEqual)

// Less than operator (<)
let isLtOrEqual = j <= k;
console.log(isLtOrEqual)

let numStr = '30';
// forced coercion to change the string to number
console.log(j > numStr); //true

let str = 'thirty'
// 50 is greater than NaN
console.log(j > str); //false

// Logical Operator
let isLegalAge = true;
let isRegistered = false;

// Logical and Operator (&&)
// Return true if all operands are true
// true && true = true
// true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet)

//Logical or Operator (|| - double pipe)
// Returns true if one of the operands are true
// true || true = true
// true || false = true
// false || false = false
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet);

// logical NOT operator (! - exclamation poin)
// Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet)


